---
title: OS
date: 2019-12-16 22:42:52
---


# Windows
[Wineows启动过程](https://basicbit.cn/2017/04/13/2017-04-13-Windows%E5%90%AF%E5%8A%A8%E8%BF%87%E7%A8%8B/)
[Windows内核对象](https://basicbit.cn/2017/06/13/2017-06-13-Windows%E5%86%85%E6%A0%B8%E5%AF%B9%E8%B1%A1/)
[WinDbg & VMware 双机调试 查看内核数据结构](https://basicbit.cn/2017/01/13/2017-10-14-Windbg%E5%8F%8C%E6%9C%BA%E8%B0%83%E8%AF%95/)
[Windows地址空间](https://basicbit.cn/2016/03/20/2016-03-20-Windows%E5%9C%B0%E5%9D%80%E7%A9%BA%E9%97%B4/)
[SSDT、PEB、TEB & Hook](https://basicbit.cn/2017/06/13/2017-07-15-SSDT,PEB,TEB/)
# Linux
[Linux开机过程](https://basicbit.cn/2016/03/10/2016-03-10-Linux%20%E5%BC%80%E6%9C%BA%E8%BF%87%E7%A8%8B/)
[Linux内核(文件)](https://basicbit.cn/2016/03/12/2016-03-12-Linux%E7%AC%A6%E5%8F%B7%E6%96%87%E4%BB%B6/)
[Linux 内存地址与分段机制](https://basicbit.cn/2016/03/12/2016-03-14-Linux%E5%86%85%E5%AD%98%E5%9C%B0%E5%9D%80/)
[Linux进程管理](https://basicbit.cn/2016/03/12/2016-03-12-Linux%E8%BF%9B%E7%A8%8B%E7%AE%A1%E7%90%86/)
[Linux kernel4.0 内核环境搭建，签名问题解决](https://basicbit.cn/2018/10/16/2018-10-16-%E7%BC%96%E8%AF%91Linux%E5%86%85%E6%A0%B84.17/)
