---
layout:     post
title:      vmp 过代码校验
date:       2018-12-31
top_img: /img/1.jpg
categories:
    - Reverse
tags:
    - vmp

typora-copy-images-to: ../img
typora-root-url: ..
---


# 代码校验

简化后的代码

```assembly
START：
mov edx,dword ptr ss:[ebp]
add ebp,0x4
LOOP：
xor eax,eax
mov ecx,eax
shl eax,0x7
shr ecx,0x19
or eax,ecx
xor al,byte ptr ds:[edx]
inc edx
dec dword ptr ss:[ebp]
jnz LOOP
mov dword ptr ss:[ebp],eax
END
```

这个handler需要两个参数，分别是代码校验的地址和大小，通过xor 指令生成校验码，最后将堆栈的两个参数弹出，将校验码压栈，执行下一个handler



# 调用分析

可以通过OD的条件断点，在校验handler下断，分别打印出 vmcode(ESI) - 1 、传入的两个参数进行分析

![image-20191217205807576](/img/image-20191217205807576.png)

日志冗长，就不贴了，经分析后一共有4处调用了校验handler，校验的部分分别是文件校验，代码校验，内存校验，随机校验



这4处校验handler调用之后的校验码比对部分有一个共同点，就是它们都是对ZF标志位进行判断

还有一个点就是hash比较后的结果会存储在一个寄存器中的BL位，也就是低字节，由堆栈弹出1字节并赋值，这里说的寄存器指的是vm context中的数据，虽然没有在每个hash handler中验证，但我猜测是一致的



在前3次处理中，会在vm context中保存循环的次数：

![image-20191217211148244](/img/image-20191217211148244.png)

上图中的堆栈33F608的位置保存了循环次数为4次

这个值的位置是不国定的，需要自己手动分析



通过这个循环次数，和ZF的标志位判断就可以跳过校验部分了



# 代码分析

校验码计算是使用刚刚计算的校验码与原校验码相减，然后判断ZF标志位

运算过程如下：

![image-20191217212314663](/img/image-20191217212314663.png)

化简后是这样的：

a-b = ~(~a + b)

f1 = eflags of (~a + b)

f2 = eflags of ~(~a + b)

eflags = (f1 & 0x815) + (f2 & ~0x815)

M1 = eflags & 40

通过对M1进行右移6位取出ZF标志位并保存到vR13BL(vm context)中



上图为以前分析的一个vmp 的 hash过程的记录，从初始化到一次循环的记录，文末有下载链接，由于每一次jmp都会对vmcontext进行打乱，阅读会有些不连贯，只能作为参考



# 跳过校验部分

前三次很好跳过，直接在代码校验的handler上下断，修改循环次数为1，不要修改为0，因为在handler中还要进行减一操作，如果你不知道循环次数在vmcontext中的保存的位置，可以用条件断点相对esi的值来判断

第四次并没有保存循环次数，但是它用到了右移指令，通常情况下，vmp会使用右移四位来判断zf位，如果zf位为1，那么右移4位后结果为4，否则为0

那么在第4次处理时首先在校验handler下断，断下后在右移handler下断，一般来说，第一次的右移是对hash校验码的判断，第二次右移是对循环的判断，所以当右移handler第二次断下，这个handler的结束会将生成的数值压栈，我们要做的就是把这个值修改为0，就可以跳过代码校验了



如果还是被检查出来了，问题就出在了每次处理的第一次检查的数据对比中，可以参考第四次的处理，在shr的handler中处理这个问题



vmp分析记录：

https://basicbit.cn/doc/Vmp%20hash%20init%20analysis.docx

https://basicbit.cn/doc/Hash%20loop.docx















