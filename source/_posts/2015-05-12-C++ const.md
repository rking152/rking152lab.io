---
layout:     post
title:      C++ 重点笔记 01
subtitle:   const 、constexpr、auto
date:       2015-05-12
author:     BY
top_img: img/c-11.png
categories:
    - Develop
tags:
	- C++
typora-root-url: ..
typora-copy-images-to: ../img
---



 C++ 重点笔记 01



# const变量

## const 定义时初始化

正确初始化示例：

```C++
int i = 1;

const int j = i;

const int j = 10;

int n  = j;
```



## 多文件使用

```c++
//file1.cpp 

extern const int buff_size = 512;

//file1.h

extern const buff_size;
```

想在多文件中共享const变量，就要加上extern。

# const引用

## const引用初始化

```c++
const int ci = 1024;

const int &ri = ci;     //正确

ri = 42 ;     //错误

int &rj = ci;  // 错误
```

![1539182417721](/img/1539182417721.png)

如果int类型引用初始化时赋值为一个double类型的数据，那么编译器会怎么做。

```c++
double i = 3.14;

const int &j = i;
```

这总情况下编译器会创建一个临时变量，如：

```c++
const int temp = i;

const int &j = i; 
```

对const的引用可能引用一个并非const的对象

```c++
int i  = 42;

int &r1 = i;

const int &r2 = i;

r1 = 0; 合法

r2 = 0; 不合法
```



## 指针与const

```c++
const  double pi = 3.14;

double *ptr = 3.14;// 不合法

const double *cptr = &pi ; //合法

*cptr  = 0; //不合法



int num = 0;

int * const 	curNum = &num ;// curNum会一直指向num

const double pi = 3.14;

const double * const ptr = &pi ;   // ptr是一个指向常量对象的常量指针

*ptr = 2.01 ;// 错误 ptr是一个指向常量的指针

*curNum = 12;  // 正确
```



## 顶层const

![1539184960881](/img/1539184960881.png)

```c++
int i = 0;

int *const p1 = &i; //不能改变p1的值，这是一个顶层const。

const int ci = 42; //不能改变ci的值，这是一个底层const。

const int  *p2 = &ci;//允许改变p2的值，这是一个顶层const;

const int * const p3 = p2 ; //靠右的是顶层const 靠左的是底层const

const int &r = ci;  //声明引用的都是底层const
```



## constexpr

### 常量表达式

```c++
const int max_files = 20 ;  //max_files是常量表达式

const int limit = max_files + 1 ; //limit 是常量表达式

int staff_size = 20; //staff_size 不是常量表达式
```



## 指针和constexpr

```c++
const int * p = nullptr; p是一个指向整型常量的指针

constexpr int *q = nullptr; q是一个指向整数常量的指针

constexpr把它定义的对象置为了顶层const
```



```c++
constexptr int i = 52 ; i是整型常量

constexpr const int * p = & i; p是常量指针 也可以指向一个非常量

constexpr int *p1 = &i; p1是常量指针 
```



# AUTO

利用auto定义变量编译器会推断并指定变量类型

使用auto也可以在一条语句中声明多个变量，因为一条声明语句只能有一个基本数据类型，所以在该条语句中所有变量的初始基本数据类型都必须一样。

```c++
auto i = 3 ， *p = &i; 正确 i是整数类型  *p 是int类型指针

auto sz = 0, pz = 3.14; 错误 sz和pz的数据类型不一样
```



## 复合类型、常量、auto

```c++
int i = 0 ; int &r = i;

auto a = r;  a是一个整数 r是i的别名，i是整数

auto一般会忽略掉顶层const

const  int ci = 0; &cr = ci;

auto b =ci ; b是一个整数

auto c = 	cr ;c是一个整数 

auto d = &i; d是一个整型指针 ，整数的地址就是指向整数的指针

auto e = &ci; e是一个指向整数常量的指针，对常量对象取地址是一种底层const
```



# decltype类型指示符

```c++
decltype(f()) num = f;
```

num的类型是f函数的返回值，在这期间不会调用f函数。



![1571160018433](/img/1571160018433.png)
