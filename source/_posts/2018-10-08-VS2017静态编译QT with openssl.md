---
layout:     post
title:      VS2017静态编译QT with openssl
subtitle:   VS2017静态编译QT with openssl
date:       2018-10-08
author:     BY
top_img: /img/vs2017.jpg
categories:
    - Develop
tags:

typora-root-url: ..
---



# VS2017 静态编译QT with openssl



在GitHub已经有人写了一个很好用的脚本。



[Click me](https://github.com/fpoussin/Qt5-MSVC-Static)

![1538994302212](/img/1538994302212.png)

在根目录会看到两个lnk  对应不同版本的vs console （x86 or x64）

在这我编译的32位的版本

建议把脚本放在盘符根目录  QT会因为路经过长而出现BUG

下载脚本 双击VS2017_Win32

![1538994774433](/img/1538994774433.png)

在个console中调用qt.bat 来下载编译

在console中输入 qt download  脚本会下载并解压qt源码  和  openssl 源码 

QT的源码在最新版本的VS2017 中编译是有问题的  在这QT官方给了一个新的源码下载链接

```bash
git pull https://codereview.qt-project.org/qt/qtbase refs/changes/05/235205/7
```

替换掉脚本下载的QT源码在sources文件夹中

![1538995628677](/img/1538995628677.png)

文件夹的名字不要换

回到脚本中 执行 qt openssl  编译openssl

![1538995705184](/img/1538995705184.png)

在编译完之后 我们就可以编译qt了 在编译qt之前 还需要修改下编译的脚本

位于 tools\setup_qt.bat  

![1538995856677](/img/1538995856677.png)

找到编译代码

做一个整体替换

```bash
echo Configuring Qt...
start  %QTDIR%\configure.bat -prefix %QTINSTALLDIR% -platform %PLATFORM% ^
-opensource -confirm-license -debug-and-release -confirm-license -opengl dynamic  -static -static-runtime -no-shared ^
-qt-libpng -qt-libjpeg -qt-zlib -qt-pcre -no-compile-examples -nomake examples ^
-no-icu -optimize-size %EXTRABUILDOPTIONS% ^
-openssl-linked -I %SSLINSTALLDIR%\include -L %SSLINSTALLDIR%\lib OPENSSL_LIBS="-llibeay32 -lssleay32" ^&^& pause exit
IF %errorlevel% NEQ 0 exit /b %errorlevel%
```

回到console中  输入qt setup 

紧接着  		输入 qt build

就开始静态编译安装QT了

编译完成后默认存放目录在  c:\Qt\5.10.1 这个文件夹中

因为我们之前替换过代码  实际我们编译的版本是5.11.1 的版本

Good Job.

![1571159962140](/img/1571159962140.png)
