---
layout:     post
title:      VS2017编译 Bitcoin master
subtitle:   bitcoin-qt
date:       2018-10-08
author:     BY
top_img: /img/vs2017.jpg
categories:
    - Develop
tags:

typora-root-url: ..
typora-copy-images-to: ..\img
---



# VS2017编译 Bitcoin master with Qt

## 下载源码

```bash
git clone https://github.com/bitcoin/bitcoin
```



![1539065582759](/img/1539065582759.png)

下载Vcpkg   用来配置bitcoin需要用到的第三方库

```bash
git clone  https://github.com/Microsoft/vcpkg
```

![1539065625326](/img/1539065625326.png)

## 初始化Vcpkg 

打开cmd   

​	cd vcpkg-master 

执行 bootstrap-vcpkg.bat 进行编译 vcpkg.exe

编译结束就可以在vcpkg根目录看到vcpkg.exe了

![1539065902565](/img/1539065902565.png)

然后，运行如下命令使计算机的所有用户都可以使用vcpkg

```bash
vcpkg integrate install
```

初始化搞定

## 安装依赖

在命令行中执行

vcpkg install boost:x86-windows-static

install 安装命令

boost 库的名字

‘ : ’后面是安装库的版本  在我我们安装库的版本全部为32位静态库 如果你想安装动态库，直接干掉-static 就行了，如果编译64位，把x86换成x64。

接下来执行

```bash
vcpkg install libevent:x86windows-static
vcpkg install openssl:x86-windows-static
vcpkg install zeromq:x86-windows-static
vcpkg install berkeleydb:x86-windows-static
vcpkg install secp256k1:x86-windows-static
vcpkg install leveldb:x86-windows-static
```

这个过程会很耗费时间    建议挂一个vpn   

安装完成之后 

我们打开到Bitcoin源码目录下面的build_msvc文件夹  看到msvc-autogen.py文件

执行这个文件  python msvc-autogen.py

![1539069538346](/img/1539069538346.png)

现在我们就可以打开 build_msvc目录下的bitcoin.sln了  

现在就可以编译比特币了  

编译完之后并没有bitcoin-qt 

## 编译bitcoin-qt

### 首先我们要先编译Protobuf 库

[下载链接](https://github.com/yuanbi/VS2017-Bitcoin)

我下载的是2.6.1的版本

![1539072815818](/img/1539072815818.png)

打开C:\deps\protobuf-2.6.1\vsprojects下的protobuf.sln，生成解决方案

Protobuf库是用来把paymentrequest.proto文件生成对应的.h、.cc文件的

打开命令行 进入C:\bitcoin-master\src\qt 目录

在这里执行protoc.exe  

命令格式

```bash
protoc --proto_path=e:/bitcoin/qt --cpp_out=e:/bitcoin/qt e:/bitcoin/qt/paymentrequest.proto
```

### 接下来我们要自己编译static qt

在我之前的博客有教程，在这就不说了。[电梯](https://www.hackyuan.top/2018/10/08/VS2017%E9%9D%99%E6%80%81%E7%BC%96%E8%AF%91QT-with-openssl/)

编译完static qt之后  需要在VS2017中安装一个Qt插件

打开VS2017 选中  工具---->拓展和更新

![1539070073798](/img/1539070073798.png)

点击 联机  --  搜索Qt  -- 下载

![1539070118357](/img/1539070118357.png)

下载完成后    会出现“扩展和更新窗口”的底部会出现“更新已列入计划…”的提示信息

关闭VS2017 

会弹出安装界面  点击修改即可

![1539070250808](/img/1539070250808.png)

安装结束之后打开VS2017

在菜单栏就可以看见 Qt VS Tools 了

![1539070318094](/img/1539070318094.png)

接下来添加我们之前编译的Static Qt 到 Qt VS Tools

![1539070415782](/img/1539070415782.png)

点击Qt Options

![1539070436942](/img/1539070436942.png)

点击Add

![1539070489872](/img/1539070489872.png)

选中我们编译的Static Qt的目录

点击确定  

搞定

接下来我们需要创建一个Qt项目

![1539070779780](/img/1539070779780.png)

选中Qt Gui Application

![1539070799504](/img/1539070799504.png)

添加Bitcoin-qt 源码进来

![1539070877737](/img/1539070877737.png)

我们需要添加

bitcoin-master\src\qt下面所有文件 

bitcoin-master\src\interfaces下面所有文件 

bitcoin-master\src\bitcoin-config.h  这个文件包含了一些预定义的宏  可以在我的GitHub上面下载 [直达电梯](https://github.com/yuanbi/VS2017-Bitcoin)

添加结束之后  打开项目属性 -- C/C/++ -- 常规 -- 附加包含目录

![1539071829785](/img/1539071829785.png)

在这里需要添加 我们之前 在vcpkg 编译库的include目录

添加之前编译bitcoin-master中的编译生成目录 

我这里是       C:\bitcoin-master\build_msvc\Release

添加Bitcoin-master\src  比特币源码目录

添加Qt plugins 目录

添加protobuf-2.6.1\src  protocbuf源码目录

![1539073087038](/img/1539073087038.png)

接下来选中代码生成 -- 运行库  修改为 多线程-MT

![1539073463472](/img/1539073463472.png)

选中 预处理器 -- 预处理器定义

添加下面的宏进去 

```
WIN32
_WIN32_WINDOWS
HAVE_WORKING_BOOST_SLEEP
LEVELDB_PLATFORM_WINDOWS
OS_WIN
ENABLE_WALLET
_CRT_SECURE_NO_WARNINGS
_SCL_SECURE_NO_WARNINGS
HAVE_CONFIG_H
_X86_
```

![1539073560643](/img/1539073560643.png)

接着 选中 配置属性 -- 链器 -- 常规 --附加库目录

添加Qt plugins 目录

添加之前编译bitcoin-master中编译的lib目录

我这里是 C:\bitcoin-master\build_msvc\Release

添加qtserialport lib目录 这个如果没有  自行下载编译 

添加Protocbuf下lib 目录

我这里的目录是

C:\qtserialport\build-qtserialport-Qt_5_11_1_msvc2017_x86_static_temporary-Release\lib

在这里需要添加 我们之前 在vcpkg 编译库的lib目录

![1539073929179](/img/1539073929179.png)

接着选中连接器 -- 输入 --  附加依赖项

添加如下lib

Qt5ThemeSupport.lib
qflatpak.lib
Qt5FbSupport.lib
Qt5EventDispatcherSupport.lib
shell32.lib
dwmapi.lib
oleaut32.lib
Qt5Widgets.lib
Qt5Gui.lib
Qt5Core.lib
Mincore.lib
crypt32.lib
Qt5OpenGLExtensions.lib
Qt5FontDatabaseSupport.lib
Qt5WindowsUIAutomationSupport.lib
Qt5OpenGL.lib
qtlibpng.lib
qtuiotouchplugin.lib
windowsprintersupport.lib
Qt5Xml.lib
qgenericbearer.lib
qgif.lib
qico.lib
qjpeg.lib
qsqlodbc.lib
qsqlite.lib
qwindowsvistastyle.lib
qdirect2d.lib
libEGL.lib
libGLESv2.lib
qtfreetype.lib
qtmain.lib
qtpcre2.lib
qtharfbuzz.lib
qminimal.lib
qoffscreen.lib
qwindows.lib
imm32.lib
winmm.lib
opengl32.lib
ws2_32.lib
wsock32.lib
Iphlpapi.lib
libbitcoin_crypto.lib
libbitcoin_cli.lib
libbitcoin_server.lib
libbitcoin_wallet.lib
libbitcoin_zmq.lib
libbitcoin_common.lib
libbitcoin_util.lib
libunivalue.lib
libbitcoin_consensus.lib
boost_filesystem-vc140-mt.lib
libprotoc.lib
libprotobuf.lib
boost_atomic-vc140-mt.lib
boost_chrono-vc140-mt.lib
boost_container-vc140-mt.lib
boost_context-vc140-mt.lib
boost_contract-vc140-mt.lib
boost_coroutine-vc140-mt.lib
boost_date_time-vc140-mt.lib
boost_exception-vc140-mt.lib
boost_fiber-vc140-mt.lib
boost_graph-vc140-mt.lib
boost_iostreams-vc140-mt.lib
boost_locale-vc140-mt.lib
boost_log-vc140-mt.lib
boost_log_setup-vc140-mt.lib
boost_math_c99-vc140-mt.lib
boost_math_c99f-vc140-mt.lib
boost_math_c99l-vc140-mt.lib
boost_math_tr1-vc140-mt.lib
boost_math_tr1f-vc140-mt.lib
boost_math_tr1l-vc140-mt.lib
boost_program_options-vc140-mt.lib
boost_python36-vc140-mt.lib
boost_random-vc140-mt.lib
boost_regex-vc140-mt.lib
boost_serialization-vc140-mt.lib
boost_signals-vc140-mt.lib
boost_stacktrace_noop-vc140-mt.lib
boost_stacktrace_windbg-vc140-mt.lib
boost_stacktrace_windbg_cached-vc140-mt.lib
boost_system-vc140-mt.lib
boost_thread-vc140-mt.lib
boost_timer-vc140-mt.lib
boost_type_erasure-vc140-mt.lib
boost_unit_test_framework-vc140-mt.lib
boost_wave-vc140-mt.lib
boost_wserialization-vc140-mt.lib
zlib.lib
ssleay32.lib
secp256k1.lib
python36.lib
lzma.lib
libzmq-mt-s-4_3_1.lib
libeay32.lib
libdb48.lib
leveldb.lib
event_extra.lib
event_core.lib
event.lib
bz2.lib
Shlwapi.lib
Qt5Network.lib
glu32.lib
Qt5SerialPort.lib
Qt5Sql.lib
Qt5Concurrent.lib
Qt5DBus.lib
Qt5PrintSupport.lib

![1539073995808](/img/1539073995808.png)

点击确定 

接下来就可以编译了

![1539074425293](/img/1539074425293.png)

![1539074472422](/img/1539074472422.png)

Good Job.

![1571159956351](/img/1571159956351.png)
